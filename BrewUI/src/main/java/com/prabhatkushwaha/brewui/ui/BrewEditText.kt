package com.prabhatkushwaha.brewui.ui

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatEditText
import com.prabhatkushwaha.brewui.R

class BrewEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatEditText(context, attrs, defStyleAttr) {

    init {
        setDefStyle(context, attrs)
    }

    private fun setDefStyle(context: Context, attrs: AttributeSet?) {
        val attributeArray: TypedArray =
            context.theme.obtainStyledAttributes(attrs, R.styleable.BrewEditText, 0, 0)
        val style: Int
        val drawable: Int
        if (attributeArray.getString(R.styleable.BrewEditText_OutlineRequired)?.toBoolean() == true
        ) {
            drawable = R.drawable.bg_edittext
        } else {
            drawable = R.drawable.bg_edittext
        }
        background = AppCompatResources.getDrawable(context, drawable)
    }
}