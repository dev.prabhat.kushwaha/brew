package com.prabhatkushwaha.brewui.ui

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import androidx.annotation.AttrRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatButton
import com.prabhatkushwaha.brewui.R

class BrewButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    @AttrRes defStyleAttr: Int = R.attr.brewButton
) : AppCompatButton(context, attrs, defStyleAttr) {

    init {
        setDefStyle(context, attrs)
    }

    private fun setDefStyle(context: Context, attrs: AttributeSet?) {
        val attributeArray: TypedArray =
            context.theme.obtainStyledAttributes(attrs, R.styleable.BrewButton, 0, 0)
        val style: Int
        val drawable: Int
        when (attributeArray.getString(R.styleable.BrewButton_ButtonType)?.toInt()) {
            0 -> {
                style = R.style.BrewButtonStyle
                drawable = R.drawable.bg_button
            }
            else -> {
                style = R.style.BrewButtonStyle
                drawable = R.drawable.bg_button
            }
        }
        isClickable= true
        setTextAppearance(style)
        background = AppCompatResources.getDrawable(context, drawable)
    }

}