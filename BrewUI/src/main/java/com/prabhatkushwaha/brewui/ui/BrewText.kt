package com.prabhatkushwaha.brewui.ui

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import com.google.android.material.textview.MaterialTextView
import com.prabhatkushwaha.brewui.R

class BrewText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialTextView(context, attrs, defStyleAttr) {

    init {
        setDefStyle(context, attrs)
    }

    private fun setDefStyle(context: Context, attrs: AttributeSet?) {

        val attributeArray: TypedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.BrewText, 0, 0)
        var style = 0
        when (attributeArray.getString(R.styleable.BrewText_Type)?.toInt()) {
            0 -> style = R.style.BrewTitleSmall
            1 -> style = R.style.BrewTitleMedium
            2 -> style = R.style.BrewTitleLarge
            3 -> style = R.style.BrewClickableStyle
            4 -> style = R.style.BrewBodySmall
            5 -> style = R.style.BrewBodyMedium
            6 -> style = R.style.BrewBodyLarge
            7 -> style = R.style.BrewLabelSmall
            8 -> style = R.style.BrewLabelMedium
            9 -> style = R.style.BrewLabelLarge
            10 -> style = R.style.BrewDisplaySmall
            11 -> style = R.style.BrewDisplayMedium
            12 -> style = R.style.BrewDisplayLarge

        }
        setTextAppearance(style)
    }
}
