package com.prabhatkushwaha.di

import com.prabhatkushwaha.brew.MainViewModel
import com.prabhatkushwaha.brewcore.di.shared.AppPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideMainViewModel(
        appPreferences: AppPreferences
    ): MainViewModel {
        return MainViewModel(appPreferences)
    }
}