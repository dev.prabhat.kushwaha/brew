package com.prabhatkushwaha.brew

import android.app.Application
import com.prabhatkushwaha.brewcore.config.AppConfig
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        initialize()
    }
    private fun initialize(){
        AppConfig.isOfflineBuild=BuildConfig.IS_OFFLINE_BUILD
    }
}