package com.prabhatkushwaha.brew

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.prabhatkushwaha.brewcore.di.shared.AppPreferences
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class MainViewModel @Inject constructor(private val appPref: AppPreferences) : ViewModel() {

    private val _uiState = MutableStateFlow(MainActivityState())
    val uiState: StateFlow<MainActivityState> = _uiState

    init {
        initialize()
    }

    private fun initialize() {
        viewModelScope.launch {
            _uiState.value = MainActivityState(
                    isLogin = appPref.isLogin(),
                    shouldNavigateToOnBoarding = !appPref.isOnBoardingAccepted(),
                    shouldNavigateToAuth = !appPref.isLogin() && appPref.isOnBoardingAccepted()
                )
        }
    }
}

class MainActivityState(
    val isLogin: Boolean = false,
    val shouldNavigateToOnBoarding: Boolean = false,
    val shouldNavigateToAuth: Boolean = false
)