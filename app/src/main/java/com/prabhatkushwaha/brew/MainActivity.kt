package com.prabhatkushwaha.brew

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.prabhatkushwaha.brewauth.auth.AuthActivity
import com.prabhatkushwaha.brewauth.onboarding.OnBoardingActivity
import com.prabhatkushwaha.brewcore.utils.ViewUtils.launch
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bindUI()
    }


    private fun bindUI() {
        lifecycleScope.launch {
            viewModel.uiState.collect { uiState ->
                if (uiState.isLogin) navigateToDashBoard()
                else if (uiState.shouldNavigateToOnBoarding) navigateToOnBoarding()
                else if (uiState.shouldNavigateToAuth) navigateToAuth()
            }
        }

    }

    private fun navigateToDashBoard() {
        startActivity(AuthActivity::class.java)
    }

    private fun navigateToOnBoarding() {
        startActivity(OnBoardingActivity::class.java)
    }

    private fun navigateToAuth() {
        startActivity(AuthActivity::class.java)
    }

    private inline fun <reified T> startActivity(destination: Class<T>) {
        Handler(Looper.getMainLooper()).postDelayed({
            launch(destination)
            finish()
        }, 1000)
    }
}