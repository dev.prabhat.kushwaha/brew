package com.prabhatkushwaha.brewauth.onboarding.screens

import android.view.LayoutInflater
import android.view.ViewGroup
import com.prabhatkushwaha.brewauth.databinding.FragmentSecondScreenBinding
import com.prabhatkushwaha.brewcore.base.BaseFragment

class SecondScreenFragment : BaseFragment<FragmentSecondScreenBinding>() {

    override fun initialize() {

    }

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentSecondScreenBinding {
        return FragmentSecondScreenBinding.inflate(inflater,container,false)
    }

}