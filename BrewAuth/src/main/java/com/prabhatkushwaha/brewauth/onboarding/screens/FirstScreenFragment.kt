package com.prabhatkushwaha.brewauth.onboarding.screens

import android.view.LayoutInflater
import android.view.ViewGroup
import com.prabhatkushwaha.brewauth.databinding.FragmentFirstScreenBinding
import com.prabhatkushwaha.brewcore.base.BaseFragment

class FirstScreenFragment : BaseFragment<FragmentFirstScreenBinding>() {
    override fun initialize() {

    }

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentFirstScreenBinding {
        return FragmentFirstScreenBinding.inflate(inflater,container,false)
    }
}