package com.prabhatkushwaha.brewauth.onboarding

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.prabhatkushwaha.brewauth.databinding.ActivityOnBoardingBinding
import com.prabhatkushwaha.brewauth.onboarding.screens.FirstScreenFragment
import com.prabhatkushwaha.brewauth.onboarding.screens.SecondScreenFragment
import com.prabhatkushwaha.brewauth.onboarding.screens.ThirdScreenFragment
import com.prabhatkushwaha.brewui.anim.ZoomOutPageTransformer
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnBoardingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnBoardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        bindUI()
    }

    private fun bindUI() {
        if (::binding.isInitialized) {
            val onBoardingAdapter = OnBoardingPageAdapter(
                this,
                listOf(FirstScreenFragment(), SecondScreenFragment(), ThirdScreenFragment())
            )
            binding.vpOnBoarding.apply {
                adapter = onBoardingAdapter
                setPageTransformer(ZoomOutPageTransformer())
                binding.wormDotsIndicator.attachTo(this)
            }

        }
    }
}
