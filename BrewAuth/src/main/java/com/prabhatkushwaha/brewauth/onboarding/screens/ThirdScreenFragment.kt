package com.prabhatkushwaha.brewauth.onboarding.screens

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.prabhatkushwaha.brewauth.auth.AuthActivity
import com.prabhatkushwaha.brewauth.databinding.FragmentThirdScreenBinding
import com.prabhatkushwaha.brewcore.base.BaseFragment
import com.prabhatkushwaha.brewcore.di.shared.AppPreferences
import com.prabhatkushwaha.brewcore.utils.ViewUtils.launch
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ThirdScreenFragment : BaseFragment<FragmentThirdScreenBinding>() {

    @Inject
    lateinit var appPref: AppPreferences

    override fun initialize() {
        binding.btGetStarted.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                appPref.setOnBoardingAccepted(true)
                navigateToAuth()
            }
        }
    }

    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentThirdScreenBinding {
        return FragmentThirdScreenBinding.inflate(inflater, container, false)
    }

    private fun navigateToAuth() {
        activity?.launch(AuthActivity::class.java)
        activity?.finish()
    }

}