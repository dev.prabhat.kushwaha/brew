package com.prabhatkushwaha.brewauth.onboarding

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import dagger.hilt.android.AndroidEntryPoint

class OnBoardingPageAdapter(activity: FragmentActivity, private val fragments:List<Fragment>):FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
      return  fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}