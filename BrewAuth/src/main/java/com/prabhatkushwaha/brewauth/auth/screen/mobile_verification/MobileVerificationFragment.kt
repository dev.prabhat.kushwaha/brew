package com.prabhatkushwaha.brewauth.auth.screen.mobile_verification

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.prabhatkushwaha.brewauth.R
import com.prabhatkushwaha.brewauth.databinding.FragmentMobileEntryBinding
import com.prabhatkushwaha.brewauth.databinding.FragmentMobileVerificationBinding
import com.prabhatkushwaha.brewcore.base.BaseFragment


class MobileVerificationFragment : BaseFragment<FragmentMobileVerificationBinding>() {
    override fun initialize() {
        binding.btContinue.setOnClickListener {
            findNavController().navigate(MobileVerificationFragmentDirections.actionMobileVerificationFragmentToPersonalDetailsFragment())
        }
    }

    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMobileVerificationBinding {
        return FragmentMobileVerificationBinding.inflate(inflater, container, false)
    }

}