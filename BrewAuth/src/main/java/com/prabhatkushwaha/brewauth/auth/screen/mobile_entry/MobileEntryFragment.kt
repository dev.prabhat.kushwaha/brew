package com.prabhatkushwaha.brewauth.auth.screen.mobile_entry

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.prabhatkushwaha.brewauth.R
import com.prabhatkushwaha.brewauth.databinding.FragmentMobileEntryBinding
import com.prabhatkushwaha.brewcore.base.BaseFragment


class MobileEntryFragment : BaseFragment<FragmentMobileEntryBinding>() {
    override fun initialize() {
        binding.btContinue.setOnClickListener {
            findNavController().navigate(MobileEntryFragmentDirections.actionMobileEntryFragmentToMobileVerificationFragment())
        }
    }

    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMobileEntryBinding {
        return FragmentMobileEntryBinding.inflate(inflater, container, false)
    }

}

