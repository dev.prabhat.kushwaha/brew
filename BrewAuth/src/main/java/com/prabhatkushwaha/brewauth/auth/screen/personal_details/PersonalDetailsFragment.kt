package com.prabhatkushwaha.brewauth.auth.screen.personal_details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.prabhatkushwaha.brewauth.R
import com.prabhatkushwaha.brewauth.databinding.FragmentPersonalDetailsBinding
import com.prabhatkushwaha.brewcore.base.BaseFragment

class PersonalDetailsFragment : BaseFragment<FragmentPersonalDetailsBinding>() {

    override fun initialize() {

    }

    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentPersonalDetailsBinding {
       return FragmentPersonalDetailsBinding.inflate(inflater,container,false)
    }

}