package com.prabhatkushwaha.brewcore.utils

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent

object ViewUtils {
    inline fun <reified T> Activity.launch(destination: Class<T>) {
        startActivity(
            Intent(this, destination),
            ActivityOptions.makeSceneTransitionAnimation(this).toBundle()
        )
    }

}