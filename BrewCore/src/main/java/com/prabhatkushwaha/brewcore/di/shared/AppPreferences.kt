package com.prabhatkushwaha.brewcore.di.shared

import androidx.datastore.core.DataStore
import com.prabhatkushwaha.brewcore.di.shared.model.AppConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.serialization.builtins.serializer
import javax.inject.Inject
import javax.inject.Singleton

const val DATA_STORE_FILE_NAME = "app-config.json"

interface AppPreferences {
    suspend fun isOnBoardingAccepted(): Boolean
    suspend fun setOnBoardingAccepted(boolean: Boolean)
    suspend fun isLogin():Boolean
    suspend fun setLogin(boolean: Boolean)

}

class AppPreferencesImpl @Inject constructor(private val appPref: DataStore<AppConfig>) : AppPreferences {
    override suspend fun isOnBoardingAccepted(): Boolean {
        return appPref.data.first().isOnBoardingAccepted
    }

    override suspend fun setOnBoardingAccepted(boolean: Boolean) {
        appPref.updateData {
            it.copy(isOnBoardingAccepted = true)
        }
    }

    override suspend fun isLogin(): Boolean {
        return appPref.data.first().isLoginUser
    }

    override suspend fun setLogin(boolean: Boolean) {
        appPref.updateData {
            it.copy(isLoginUser = true)
        }
    }

}
