package com.prabhatkushwaha.brewcore.di.shared.model

import kotlinx.serialization.Serializable

@Serializable
data class AppConfig(
    val isOnBoardingAccepted: Boolean = false,
    val isLoginUser: Boolean = false
)