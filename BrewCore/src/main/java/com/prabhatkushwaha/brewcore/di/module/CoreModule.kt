package com.prabhatkushwaha.brewcore.di.module

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.core.DataStoreFactory
import androidx.datastore.dataStoreFile
import com.prabhatkushwaha.brewcore.di.shared.AppPreferences
import com.prabhatkushwaha.brewcore.di.shared.AppPreferencesImpl
import com.prabhatkushwaha.brewcore.di.shared.AppPreferencesSerializer
import com.prabhatkushwaha.brewcore.di.shared.DATA_STORE_FILE_NAME
import com.prabhatkushwaha.brewcore.di.shared.model.AppConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(value = [SingletonComponent::class])
class CoreModule {

    @Provides
    @Singleton
    fun provideAppPreference(
        @ApplicationContext appContext: Context
    ): AppPreferences {
        val dataStore = DataStoreFactory.create(
            serializer = AppPreferencesSerializer,
            produceFile = { appContext.dataStoreFile(DATA_STORE_FILE_NAME) },
            corruptionHandler = null
        )
        return AppPreferencesImpl(dataStore)
    }


}