package com.prabhatkushwaha.brewcore.di.shared

import androidx.datastore.core.Serializer
import com.prabhatkushwaha.brewcore.di.shared.model.AppConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import java.io.InputStream
import java.io.OutputStream

object AppPreferencesSerializer : Serializer<AppConfig> {
    override val defaultValue: AppConfig
        get() = AppConfig()

    override suspend fun readFrom(input: InputStream): AppConfig {
        return try {
            Json.decodeFromString(
                deserializer = AppConfig.serializer(),
                string = input.readBytes().decodeToString()
            )
        } catch (exception: SerializationException) {
            exception.printStackTrace()
            defaultValue
        }
    }

    override suspend fun writeTo(t: AppConfig, output: OutputStream) {
        withContext(Dispatchers.IO) {
            output.write(
                Json.encodeToString(
                    serializer = AppConfig.serializer(),
                    value = t
                ).encodeToByteArray()
            )
        }
    }
}